package org.eclipse.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PostUpdate;

@Entity
 
public class Personne {
	
	@Id
	@GeneratedValue (strategy=GenerationType.AUTO) 
	private int num;
	private NomComplet nomComplet;
	private int nbrMAJ = 0;
	
	public int getNbrMAJ() { return nbrMAJ; } 
	public void setNbrMAJ(int nbrMAJ) { this.nbrMAJ = nbrMAJ; } 
	
	@PostUpdate 
	public void updateNbrMAJ() { this.nbrMAJ++; } 
	
	public int getNum() {
		return num;
	}
	
	public NomComplet getNomComplet() {
		return nomComplet;
	}
	
	public void setNomComplet(NomComplet nomComplet) {
		this.nomComplet = nomComplet;
	}
	
	public Personne(NomComplet nomComplet) {
		super();
		this.nomComplet = nomComplet;
	}
	
	
	@Override
	public String toString() {
		return "Personne [num=" + num + ", nomComplet=" + nomComplet + ", nbrMAJ=" + nbrMAJ + "]";
	}
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}
}
